package App::GitLab::Backend;
use 5.010;
use strict;
use warnings;

use LWP::UserAgent ();
use HTTP::Request::Common qw(POST);


use Exporter qw(import);
our @EXPORT_OK = qw(run);

sub run {
    my ($token, $url, $variables) = @_;

    my %content;
    $content{token} = $token;
    $content{ref} = 'master';
    while (my ($name, $value) = each %$variables) {
        $content{"variables[$name]"} = $value;
    }

    my $request = POST $url, 'Content-Type' => 'form-data', Content => \%content;
    my $ua = LWP::UserAgent->new(timeout => 10);
    my $response = $ua->request($request);
    return $response->decoded_content;
}


1;


package App::GitLab::Runner;
use Dancer2;
use Time::HiRes qw(time);
use File::Basename qw(dirname basename);
use File::Spec::Functions qw(catfile);
use Data::Dumper qw(Dumper);
use Path::Tiny qw(path);

our $VERSION = '0.1';

use App::GitLab::Backend qw(run);

my $root = dirname(dirname(dirname(dirname(__FILE__))));
my $jobs_dir = catfile($root, 'jobs');

get '/' => sub {
    my $fill = query_parameters->get('fill');
    my $params = {};
    if ($fill and $fill =~ /^[0-9]+\.[0-9]+$/) {
        my $filename = catfile($jobs_dir, $fill) . '.json';
        if (open my $fh, '<', $filename) {
            local $/ = undef;
            my $data = from_json scalar <$fh>;
            debug $data;
            $params = $data->{params};
        }
    }
    debug $params;
    template 'index' => { 'title' => 'App::GitLab::Runner', jobparams => $params };
};

get '/job' => sub {
    my $filename = catfile( $root, 'job.yml' );
    my $job_config = from_yaml path($filename)->slurp_utf8;
    for my $field (@{ $job_config->{fields} }) {
        if (exists $field->{values}) {
            $field->{values} = [ map { {value => $_, selected => 0 } } @{ $field->{values} }  ];
        }
    }
    debug Dumper $job_config;

    template 'job' => { 'title' => 'Start job', job => $job_config };
};

get '/history' => sub {
    my @job_files = map { substr basename($_), 0, -5 } glob("$jobs_dir/*.json");
    template 'history' => { 'title' => 'App::GitLab::Runner - History', jobs => \@job_files };
};


post '/run' => sub {
    my $time = time;
    my $job_name = body_parameters->get('jobname');
    my $job_filename = catfile( $root, 'job.yml' );
    my $job_config = from_yaml path($job_filename)->slurp_utf8;
    my @fields = map { $_->{name} } @{ $job_config->{fields} };

    my %variables;
    for my $field (@fields) {
        $variables{$field} = body_parameters->get($field);
    }
    debug \%variables;

    my $config = config();
    my $url = $config->{GitLabRunner}{url};
    my $token = $config->{GitLabRunner}{token};

    my $response_str = run($token, $url, \%variables);
    my $response = from_json $response_str;
    my $filename = catfile( $jobs_dir, $time . '.json');
    if (open my $fh, '>', $filename) {
        print $fh to_json({
            params => \%variables,
            response => $response,
        });
    }

    template 'response' => { title => 'Launched', response => $response };
};

true;

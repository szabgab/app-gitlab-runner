#!/usr/bin/env perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib";


# use this block if you don't need middleware, and only have a single target Dancer app to run here
use App::GitLab::Runner;

App::GitLab::Runner->to_app;

=begin comment
# use this block if you want to include middleware such as Plack::Middleware::Deflater

use App::GitLab::Runner;
use Plack::Builder;

builder {
    enable 'Deflater';
    App::GitLab::Runner->to_app;
}

=end comment

=cut

=begin comment
# use this block if you want to mount several applications on different path

use App::GitLab::Runner;
use App::GitLab::Runner_admin;

use Plack::Builder;

builder {
    mount '/'      => App::GitLab::Runner->to_app;
    mount '/admin'      => App::GitLab::Runner_admin->to_app;
}

=end comment

=cut


use strict;
use warnings;

use App::GitLab::Runner;
use Test::More;
use Plack::Test;
use HTTP::Request::Common;

my $app = App::GitLab::Runner->to_app;
my $test = Plack::Test->create($app);

subtest main => sub {
    my $res  = $test->request( GET '/' );
    ok( $res->is_success, '[GET /] successful' );
};


my @params;

# mock the function after it was imported
*App::GitLab::Runner::run = sub {
    push @params, \@_;
    return App::GitLab::Runner::to_json $_[2];
};

subtest run_empty => sub {
    @params = ();
    my $res  = $test->request( POST '/run' );
    ok( $res->is_success, '/run' );
    like $res->content, qr/Status:/;
    #diag explain \@params;
    is_deeply $params[0][2], {
                'Z_select' => undef,
                'Z_text' => undef,
                'Z_another' => undef,
    };
};

subtest run_full => sub {
    @params = ();
    my $res  = $test->request( POST '/run', { Z_select => 'blue', Z_text => 'green' } );
    ok( $res->is_success, '/run' );
    like $res->content, qr/Status:/;
    is_deeply $params[0][2], {
                'Z_select' => 'blue',
                'Z_text' => 'green',
                'Z_another' => undef,
    };
};


done_testing;

# A GUI to launch CI processes on GitLab with parameters provided by the user.

## Plans

An HTML form that would accept
* one-line text input - we should have some input validation behind it.
* drop-down list to select from values (some will allow empty submission, some will start with an empty selection but will require you to select a value)

* Maybe even add a multi-level selector e.g.:
First select an environment: Development, Testing, or Acceptance
Then select from a list of machines that depend on the first selection.

* When selecting which tests to run the list might come from the
- list of test files in perl
- list of test cases in python pytest

* We can also have a few checkboxes and a set of radio buttons to select a value and pass in as environment variables.


* Maybe some kind of authentication so we know who ran the tests.

* Remember the test runs and be able to fill the form using an earlier configuration.
   When was it executed
   Who executed it
   What were the parameters

* Can we also check the results or at least link to the pipeline run that came from the job?


## TODO

* fix the filling of the form in the select element as well DONE
* make configuration parameters as suggested on the Dancer mailing list DONE

* Response when job was executed should be a regular html page and not the JSON we got back.

* add explanation how to use the system.
* move the list of fields out of the code to a configuration file, allow for multiple jobs on the same system.
* make the history show only a limited set of items and allow the user to "page"
* Separate process to check the results?

* Add the "fill"-ing of the jobs back to the generated page
* Allow the description of multiple jobs
* in the /job route select the job-configuration based on the (hidden) input from the user. (Verify that it exists)
* Input validation: Verify that the selected value is one of the listed values.

